#!/usr/bin/python2
import sys, os
sys.path.append(os.path.dirname(__file__))

import cv, Image
from StringIO import StringIO
from detect_draw import gentlemanize


def application(environ, start_response):
    headers = [('Content-Type', 'text/plain')]
    if environ['REQUEST_METHOD'] != "POST":
        start_response("405 POST expected", headers)
        yield "POST an image to me, please."
        return

    # Server load management goes here
    status = '200 OK'
    try:
        input_size = int(environ['CONTENT_LENGTH'])
    except (ValueError, KeyError):
        start_response("411 Length Required", headers)
        return
    
    # Everything's okay, run it.
    start_response("200 OK", headers)
    img_in = Image.open(StringIO(environ['wsgi.input'].read(input_size)))
    img_cv = cv.CreateImageHeader(img_in.size, cv.IPL_DEPTH_8U, 3)
    cv.SetData(img_cv, img_in.tostring())
    del img_in

    result = StringIO()
    monocle_set, img_cv = gentlemanize(img_cv)
    img_cv.save(result, format="png")
    result.seek(0,0)

    yield '{ "objects": ['
    for m in monocle_set:
        yield '"' + str(m) + '",'
    yield '], "image": "data:image/png;base64,'
    for line in result:
        # Drop trailing newlines
        yield memoryview(line)[:-1]
    yield '" }'

if __name__ == "__main__":
    import sys
    infile = sys.stdin.read()
    length = len(infile)
    infile = StringIO(infile)
    environ = {
            'CONTENT_LENGTH': length,
            'wsgi.input': infile
    }
    for s in application(lambda: None, environ):
        print(s)
