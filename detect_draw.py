#!/usr/bin/env python2
from __future__ import print_function
import sys, time

from functools import partial
print = partial(print, file=sys.stderr)

# Requires OpenCV
import cv
# Requires PIL
import Image, ImageDraw

## TODO: find faces, replace with trollfaces

class FeatureDetector(object):
    def __init__(self):
        self.features = {}

    def add_feature(self, cascade, name):
        self.features[name] = cv.Load(cascade)

    def detect(self, img):
        results = []

        for feature, cascade in self.features.iteritems():
            if isinstance(cascade, tuple):
                print("Cascade '{0}' already processed.".format(feature))
                continue
            # Rumor has it this is multithreaded.  I don't think so.
            print("{0}: ".format(feature), end='')
            start = time.time()
            r = cv.HaarDetectObjects(img, cascade, cv.CreateMemStorage(0), *params_fastest)
            elapsed = time.time() - start
            print("{0} seconds".format(elapsed))
            self.features[feature] = [BoundingBox(x) for x, neighbors in r]
        

def simplify_image(img):
    """Reduce the size and color depth of img for faster feature detection."""
    # Work on images 800 pixels in the largest dimension
    scale = 800.0 / (img.width if img.width > img.height else img.height)
    scale = min(scale, 1)
    # allocate temporary images
    small_img = cv.CreateImage(
            (cv.Round(img.width * scale),
             cv.Round(img.height * scale)),
             cv.IPL_DEPTH_8U, 3)
    # Dropping to one channel cuts memory needs while processing, but adds a step
    gray = cv.CreateImage((small_img.width, small_img.height), 8, 1)
    # Resize, convert color to greyscale, and renormalize contrast
    cv.Resize(img, small_img, cv.CV_INTER_LINEAR)
    cv.CvtColor(small_img, gray, cv.CV_RGB2GRAY)
    # This seems to hurt results on some test images.
    #cv.EqualizeHist(gray, gray)
    return scale, gray

class BoundingBox(object):
    def __init__(self, pts):
        self.x, self.y, self.w, self.h = pts
    def __repr__(self):
        return "({0}+{1}, {2}+{3})".format(self.x, self.w, self.y, self.h)

    def fully_contains(self, o):
        """Return true if parent completely bounds child.
           parent - 4-tuple of the form (x1, y1, x2, y2) describing a rectangle
           child - 4-tuple as in parent"""
        upper_left = o.x >= self.x and o.y >= self.y
        lower_right = (self.x + self.w) >= (o.x + o.w)
        lower_right &= (self.y + self.h) >= (o.y + o.h)
        return upper_left and lower_right

    def draw(self, img, color):
        """Draws this box on img in the given color."""
        ul = (self.x, self.y)
        lr = (self.x + self.w, self.y + self.h)
        cv.Rectangle(img, ul, lr, color, 3, 8, 0)

    def center(self):
        """Returns the centerpoint of this bounding box, (x,y) tuple."""
        return (self.x + .5 * self.w, self.y + .5 * self.h)

    def scale(self, factor):
        """
        Scales all components of this bounding box by factor, clamping
        values to integers.
        """
        self.x, self.y, self.w, self.h = map(lambda x: int(factor * x),
                (self.x, self.y, self.w, self.h))

def points_dist(p1, p2):
    """Gets the distance, as a float, between two points represented by tuples
       of the form (x,y)."""
    x1, y1 = p1
    x2, y2 = p2
    return ((x1 - x2)**2 + (y1 - y2)**2) ** .5

def draw_monocle(img, loc, mirror=False):
    """
    Draws a monocole on img
    img - PIL Image to draw on
    loc - bounding box of space monocle should cover
    mirror - if True, mirror the monocle horizontally before compositing
    """
    #print "draw_monocle(img, %s)" %str(loc)
    monocle_center = map(int, loc.center())
    monocle_radius = max(loc.w, loc.h) / 2
    #print "Centered at %s, r = %i" %(str(monocle_center), monocle_radius)
    monocle = create_monocle(monocle_radius, mirror)
    # Paste monocle into a trasparent field same size as total image
    layer = Image.new('RGBA', img.size, (0,0,0,0))
    layer.paste(monocle, (monocle_center[0]-monocle_radius,
                          monocle_center[1]-monocle_radius))
    return Image.composite(layer, img, layer)

def create_monocle(r, mirror):
    """Loads and resizes the monocle image to radius r pixels."""
    from _images import monocle
    from StringIO import StringIO
    from base64 import b64decode
    monocle = b64decode(monocle)
    monocle = StringIO(monocle)
    i = Image.open(monocle)
    i = i.transpose(Image.FLIP_LEFT_RIGHT)
    i = i.resize((2*r,2*r), Image.ANTIALIAS)
    return i

# Features to detect and relevant Haar cascade files
hc_path = "/usr/share/opencv/haarcascades/"
hc_face = ("face", "haarcascade_frontalface_default.xml")
hc_mouth = ("mouth", "haarcascade_mcs_mouth.xml")
hc_righteye = ("eye", "haarcascade_eye.xml")
features = [(n, hc_path + f) for n, f in hc_face, hc_righteye]
# True to write images for partials as well as final
gen_partials = False
# Options for HaarDetectObjects
params_default = (1.1, 1, 3, 0, (0, 0))
params_faster = (1.2, 2, 0, (20, 20))
params_fastest = (1.2, 2, cv.CV_HAAR_DO_CANNY_PRUNING, (0,0))

def gentlemanize(original):
    # Optimization: detect eyes/other features within faces only.
    # That particular one is more efficient overall, but the existing
    #  threaded implementation will usually be faster on multiprocessor
    #  machines.. maybe.
    # Rescale for performance, Need the scale factor for compositing later
    scale, img = simplify_image(original)
    #print("Downsample x{0}".format(scale))
    # Do detection
    detector = FeatureDetector()
    for name, feature in features:
        detector.add_feature(feature, name)
    detector.detect(img)

    # Map eyes to the faces contained therein
    eyemap = {}
    eyes = detector.features["eye"]
    for face in detector.features["face"]:
        contained_eyes = filter(face.fully_contains, eyes)
        if len(contained_eyes) > 0:
            eyemap[face] = contained_eyes

    # Find one eye which seems to match geometric proportions for each face
    for face, all_eyes in eyemap.iteritems():
        bestdist = sys.maxint
        besteye = None

        target_x = [face.x + face.w * d for d in (0.33, 0.66)]
        target_y = face.y + face.h * .4
        targets = zip(target_x, (target_y,) * 2)
        for eye in all_eyes:
            # Select eye with minimum distance from guessed eye locations
            eye_center = eye.center()
            delta = min((points_dist(eye_center, x) for x in targets))
            #print("Eye at {0}, delta = {1}".format(eye, delta))
            if delta < bestdist:
                bestdist = delta
                besteye = eye
        #print("Face {0} best eye at {1}".format(face, besteye))
        eyemap[face] = besteye

    # Dump results to an image if requested
    if False:
        unculled = cv.CreateImage(cv.GetSize(img), img.depth, img.nChannels)
        cv.Copy(img, unculled)
        for face, eye in eyemap.iteritems():
            face.draw(unculled, cv.RGB(255,0,0))
            eye.draw(unculled, cv.RGB(255,0,0))
        cv.SaveImage("unculled.png", unculled)
        print("Wrote unculled boxes to unculled.png")

    # Convert image to PIL and draw monocles
    img = Image.new("RGBA", (original.width, original.height))
    for face, eye in eyemap.iteritems():
        # Mirror the monocle so the nub points outward
        mirror = bool(int(((eye.x - face.x) / float(face.w)) + .5))
        #print("Monocle {0} in face {1}, mirror={2}".format(eye, face, mirror))
        # Rescale (simplify resizes) and composite
        eye.scale(1 / scale)
        img = draw_monocle(img, eye, mirror)
    # monocles, image
    return eyemap.values(), img

from StringIO import StringIO

if __name__ == '__main__':
    print("__main__")
    stdin = StringIO(sys.stdin.read())
    img_in = Image.open(stdin)
    img_cv = cv.CreateImageHeader(img_in.size, cv.IPL_DEPTH_8U, 3)
    cv.SetData(img_cv, img_in.tostring())
    out = gentlemanize(img_cv)
    out.save(sys.stdout, format="png")

